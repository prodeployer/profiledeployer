public class DeploymentHomePageControllerNew {
    public static String SourceOrgValue {get;set;}
    public static String DestinationOrgValue {get;set;}
    public String profileName {get;set;}
    public List<SelectOption> optionsListProfile {get;set;}
    public DeploymentHomePageControllerNew() {
    SourceOrgValue = null;
    optionsListProfile = new List<SelectOption>();
    }
    
    public List<SelectOption> getListSourceOrg() {
        List<SelectOption> options = new List<SelectOption>();
        List<Salesforce_Org__c> listSalesforceOrgs =  [Select id, Name From salesforce_org__c];
        system.debug('>>> listSalesforceOrgs : '+listSalesforceOrgs);
        options.clear();
        //options.add(new SelectOption('--None--','--None--'));
        for(Salesforce_Org__c objSalesforceOrg : listSalesforceOrgs) {
            options.add(new SelectOption(objSalesforceOrg.Name,objSalesforceOrg.Name));
        }
        return options;
    }
    
    public List<SelectOption> getDestinationOrgValues() {
        system.debug('>>> SourceOrgValue: '+SourceOrgValue);
        return null;
    }     
}