/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *sdv
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
public class DeploymentHomePageController {
    public static String SourceOrgValue {get;set;}
    public String profileName {get;set;}
    public List<SelectOption> optionsListProfile {get;set;}
    public static String accessToken {get;set;}
    public DeploymentHomePageController() {
    SourceOrgValue = null;
    optionsListProfile = new List<SelectOption>();
    }
    
    public List<SelectOption> getListSourceOrg() {
        List<SelectOption> options = new List<SelectOption>();
        List<Salesforce_Org__c> listSalesforceOrgs =  [Select id, Name From salesforce_org__c];
        options.clear();
        for(Salesforce_Org__c objSalesforceOrg : listSalesforceOrgs) {
            options.add(new SelectOption(objSalesforceOrg.Name,objSalesforceOrg.Name));
        }
        return options;
    }
    
    public List<SelectOption> getListProfileNames() {
        optionsListProfile = listMetadata();
        return optionsListProfile;
    }
    
    public static MetadataService.MetadataPort createService(String strOrgName) {
        if(strOrgName!=Null) {
        MetadataService.MetadataPort service = new MetadataService.MetadataPort();
        service.SessionHeader = new MetadataService.SessionHeader_element();
        System.debug('Seeseion ID' + strOrgName);
        service.SessionHeader.sessionId = DMLUtility.retrieveAccessToken(strOrgName);
        System.debug('Seeseion ID + ' + service.SessionHeader.sessionId);
        return service;
        }
        return null;
    }
    
    public static List<SelectOption> listMetadata()
    {
        List<SelectOption> options = new List<SelectOption>();
        options.clear();
        MetadataService.MetadataPort service = createService(SourceOrgValue);
        
        system.debug(service);
        List<MetadataService.ListMetadataQuery> queries = new List<MetadataService.ListMetadataQuery>();        
        MetadataService.ListMetadataQuery queryWorkflow = new MetadataService.ListMetadataQuery();
        queryWorkflow.type_x = 'Profile';
        queries.add(queryWorkflow);     
        
        MetadataService.FileProperties[] fileProperties = service.listMetadata(queries, 25);
        integer i = 0;
        for(MetadataService.FileProperties fileProperty : fileProperties){
            options.add(new SelectOption(fileProperty.fullName,fileProperty.fullName));
            System.debug(i+' : '+fileProperty.fullName);
            System.debug(i+' : '+fileProperty);
            i++;
            
        }
        system.debug(options);
        return options;
    }
    
    public void retrieveProfile() {
        String MetaDataType = 'Profile';

        //For demo, apex class AccountManager is used.
        String MetaDataItem = profileName; 

        final Integer METADATA_API_VERSION = Integer.valueOf(new MetadataService.MetadataPort().endpoint_x.substringAfterLast('/')); 

        //Creating instance for metadataport. 
        
        MetadataService.MetadataPort service = createService(SourceOrgValue); 
        
        MetadataService.RetrieveRequest retrieveRequest = new MetadataService.RetrieveRequest(); 
        
        retrieveRequest.apiVersion = METADATA_API_VERSION; 
        
        retrieveRequest.packageNames = null;
        
        retrieveRequest.singlePackage = true;
        
        retrieveRequest.specificFiles = null;
        
        retrieveRequest.unpackaged = new MetadataService.Package_x();
        
        retrieveRequest.unpackaged.types = new List<MetadataService.PackageTypeMembers>();
        
        MetadataService.PackageTypeMembers packageType = new MetadataService.PackageTypeMembers();
        
        packageType.name = MetaDataType;
        
        packageType.members = new String[] { MetaDataItem }; 
        
        retrieveRequest.unpackaged.types.add(packageType); 
        system.debug('Service : '+service);
        
        MetadataService.AsyncResult AsyncResult = service.retrieve(retrieveRequest); 
        
        //Asynchrnous calls will not provide zip file immediately.  Poll the service with some time interval to fetch the zipfile. 
        Long startingTime = System.now().getTime(); // Num milliseconds since Jan 1 1970
        Integer delayInMilliseconds = 15000; // One-second delay
        while (System.now().getTime() - startingTime < delayInMilliseconds)  {
                    // Do nothing until desired delay has passed
         }
        
        MetadataService.RetrieveResult retrieveResult = service.checkRetrieveStatus(AsyncResult.Id, true); 
        
        system.debug('retrieveResult base64 encoded string'+retrieveResult.zipFile); 
        system.debug('retrieveResult'+retrieveResult); 
        
         try{
              Document doc = new Document();
              doc.FolderId = UserInfo.getUserId();
              doc.Name = 'Metadata';
              doc.Body = System.EncodingUtil.base64Decode(retrieveResult.zipFile);
              doc.type = 'zip';
              insert doc;
              System.debug('>>> doc ' + doc.Id);
              
            } catch ( Exception ex ) {
              System.debug('>>> ERROR ' + ex);
        }
    }
}