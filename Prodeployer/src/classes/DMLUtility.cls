public with sharing class DMLUtility {
    public static String retrieveAccessToken(String strOrgName) {
        String strAccessToken;
        Salesforce_Org__c objSalesforceOrg = [Select Access_Token__c 
                                                FROM Salesforce_Org__c 
                                                WHERE Name = : strOrgName
                                                LIMIT 1
                                            ];
        strAccessToken = objSalesforceOrg.Access_Token__c;
        return strAccessToken;
    }
}