/**************************************************************************************************
 * Class : OrgRegistration
 * Created By : Ashwini Borse
 --------------------------------------------------------------------------------------------------
 * Description : Controller to org registration VF page. .
 --------------------------------------------------------------------------------------------------
 * Version History:
 * Version  Developer Name    Date          Detail Features
 * 1.0      Ashwini Borse   05/05/2017    Initial Development
 **************************************************************************************************/
public with sharing class OrgRegistration {
    ApexPages.StandardController controllerref;
    public String currentType{get;set;}
    public Salesforce_Org__c sfdcOrgObj {get;set;}
    public UserWrapper wrapper {get;set;}
    public Salesforce_Org__c insertObjSalesforce_Org;
    public String strAuthToken;
    public String strOrgName;
    public String strOrgType;
    public String strOrgURL;

    public OrgRegistration(ApexPages.StandardController controller) {
        controllerref=controller;
        sfdcOrgObj = (Salesforce_Org__c) controller.getRecord();
        if(Apexpages.currentpage().getParameters().containskey('code')){
            strAuthToken = Apexpages.currentpage().getParameters().get('code');
            String strMyState = Apexpages.currentpage().getParameters().get('state');
            String[] strMyStates  = strMyState.split(';');
            system.debug(strAuthToken );
            strOrgName = strMyStates[0];
            strOrgType = strMyStates[1];
            strOrgURL = 'https://' + strMyStates[2];
            system.debug(strMyStates);
            system.debug(strOrgName);
            system.debug(strOrgURL);
            system.debug(strAuthToken );
            //insert insertObjSalesforce_Org;
        }
    }
    public PageReference cancel(){
    PageReference newpage = new PageReference(System.currentPageReference().getURL());
    newpage.setRedirect(true);
    return newpage;
}
    /**
    * Method to submit the org registration page.
    */
    public PageReference registerOrg(){
        String strMyState = sfdcOrgObj.Org_Name__c + ';' + sfdcOrgObj.Org_Type__c + ';' + sfdcOrgObj.Org_Url__c ;
                
        HttpRequest httpRequest = new HttpRequest();
        httpRequest.setEndpoint('https://' + sfdcOrgObj.Org_URL__c + '/services/oauth2/authorize');
        httpRequest.setMethod('POST');
        httpRequest.setBody('state=' + strMyState + '&response_type=code&client_id=' + System.Label.Consumer_Key + '&redirect_uri=' + System.Label.Redirect_Uri + '&display=popup');
        Http http = new Http();
        HTTPResponse httpResponse = http.send(httpRequest);
        system.debug('response body : ' + httpResponse.getHeader('Location'));
        
        PageReference objPageReference = new PageReference(httpResponse.getHeader('Location'));+
        objPageReference.setRedirect(true);
        system.debug('Stop' + objPageReference.getParameters().get('code'));
        return objPageReference;
    }
    /**
    * Method to reset the org registration page.
    */
    public PageReference reset() {
        PageReference newpage = new PageReference(System.currentPageReference().getURL());
        newpage.setRedirect(true);
        return newpage;
    }

    public void updateUrl() {
        currentType = System.currentPageReference().getParameters().get('paramEnvType');
        strOrgName = sfdcOrgObj.Org_Name__c;
        strOrgType = sfdcOrgObj.Org_Type__c;
    system.debug('>>> currentType'+currentType);
        if(currentType=='Production or Development') {
            sfdcOrgObj.Org_URL__c = 'login.salesforce.com';
        }
        else if(currentType=='Sandbox') {
            sfdcOrgObj.Org_URL__c = 'test.salesforce.com';
        }
        else if(currentType=='Pre-Release') {
            sfdcOrgObj.Org_URL__c = 'prerellogin.pre.salesforce.com';
        }
    }
    
    public static MetadataService.MetadataPort createService() {
		MetadataService.MetadataPort service = new MetadataService.MetadataPort();
		service.SessionHeader = new MetadataService.SessionHeader_element();
		service.SessionHeader.sessionId = UserInfo.getSessionId();
		return service;
    }
    
    public static void createRemoteSiteSettings(String OrgName, String RemoteSiteURL) {
        MetadataService.MetadataPort service = createService();
	    MetadataService.RemoteSiteSetting remoteSiteSettings = new MetadataService.RemoteSiteSetting();
	    remoteSiteSettings.fullName = 'ProDep_' + OrgName;
	    remoteSiteSettings.url = RemoteSiteURL;
	    remoteSiteSettings.description = 'ProDeployer Remote Site : ' + OrgName;
	    remoteSiteSettings.isActive=true;
	    remoteSiteSettings.disableProtocolSecurity=false;
	    MetadataService.SaveResult[] results = service.createMetadata(new List<MetadataService.Metadata> { remoteSiteSettings });
	    //MetadataService.AsyncResult[] checkResults = service.checkStatus(new List<string> {string.ValueOf(results[0].Id)});
	    system.debug('chk' + String.valueOf(results[0].Success));
	}
    
    public PageReference CreateRecord(){
        system.debug(strAuthToken);
        if(String.isNotBlank(strAuthToken)){
        HttpRequest objHttpRequest = new HttpRequest();
            objHttpRequest.setEndpoint( strOrgURL +'/services/oauth2/token');
            objHttpRequest.setMethod('POST');
            objHttpRequest.setBody('grant_type=' + System.Label.Grant_Type + '&code=' + strAuthToken + '&client_id=' + System.Label.Consumer_Key + '&client_secret=' + System.Label.Secret_Key + '&redirect_uri=' + System.Label.Redirect_Uri);
            Http objHttp = new Http();
            HTTPResponse objHTTPResponse = objHttp.send(objHttpRequest);
            system.debug('response body : '+ objHTTPResponse.getBody());
            responseWrapper objresponseWrapper = (responseWrapper)JSON.deserialize(objHTTPResponse.getBody(),responseWrapper.class);
            system.debug('response access_token : '+ objresponseWrapper.access_token);
            system.debug('response refresh_token : '+ objresponseWrapper.refresh_token);
            system.debug('response instance_url : '+ objresponseWrapper.instance_url);
            system.debug('response token_type : '+ objresponseWrapper.token_type);
            system.debug('response issued_at : '+ objresponseWrapper.issued_at);
            system.debug('response id : '+ objresponseWrapper.id);
            Salesforce_Org__c insertObjSalesforce_Org = new Salesforce_Org__c();
            system.debug(strOrgName);
            system.debug(strOrgType);
            
            // Identity URL : Get User Detail : Starts Here
            HTTPRequest objHTTPRequests = new HTTPRequest();
			objHTTPRequests.setHeader('Authorization', 'Bearer ' + objresponseWrapper.access_token);
			objHTTPRequests.setEndpoint(objresponseWrapper.id);
			objHTTPRequests.setMethod('GET');
			HTTP objHTTPs = new HTTP();
			HTTPResponse objHTTPResponses = objHTTPs.send(objHTTPRequests);
			system.debug('>>> body: '+objHTTPResponses.getBody());
			
			wrapper = (UserWrapper)JSON.deserialize(objHTTPResponses.getBody(), UserWrapper.class);
			system.debug('>>> wrapper: '+wrapper);
            // Identity URL : Get User Detail : Ends Here
            
            
            insertObjSalesforce_Org.id__c = objresponseWrapper.id;
            insertObjSalesforce_Org.access_token__c = objresponseWrapper.access_token ;
            insertObjSalesforce_Org.refresh_token__c = objresponseWrapper.refresh_token ;
            insertObjSalesforce_Org.instance_url__c = objresponseWrapper.instance_url ;
            insertObjSalesforce_Org.token_type__c = objresponseWrapper.token_type ;
            insertObjSalesforce_Org.issued_at__c = objresponseWrapper.issued_at ;
            insertObjSalesforce_Org.Org_URL__c = strOrgURL;
            insertObjSalesforce_Org.Org_Name__c = strOrgName;
            insertObjSalesforce_Org.Org_Type__c = strOrgType;
            insertObjSalesforce_Org.Registration_Type__c = sfdcOrgObj.Registration_Type__c ;
            insertObjSalesforce_Org.Name = strOrgName;
            insertObjSalesforce_Org.registration_type__c = 'OAuth';
            insert insertObjSalesforce_Org;
            
            PageReference newPr = new PageReference('/' + insertObjSalesforce_Org.Id);
            return newPr;
            }
            else{
                return null;
            }
    }

}